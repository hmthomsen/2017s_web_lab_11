-- Questions

-- Develop SQL statements that answer the following questions, placing the answers in the ex2/ex2.sql file of the project:
-- A. What are the names of the students who attend COMP219?
SELECT fname, lname, num
  FROM unidb_students, unidb_courses
  WHERE unidb_courses.num = '219';

-- B. What are the names of the student reps that are not from NZ?
SELECT *
  FROM unidb_students
  WHERE country NOT LIKE 'NZ';

-- C. Where are the offices for the lecturers of 219?
SELECT office, num
  FROM unidb_lecturers, unidb_courses
  WHERE unidb_courses.num = '219';

-- D. What are the names of the students taught by Te Taka?

-- E. List the students and their mentors
-- not happy with this one yet
SELECT unidb_students.fname, unidb_students.lname, unidb_students.mentor, unidb_lecturers.staff_no
  FROM unidb_students, unidb_lecturers
  ORDER BY mentor;
-- F. Name the lecturers whose office is in G-Block as well naming the students that are not from NZ

-- G. List the course co-ordinator and student rep for COMP219
SELECT unidb_courses.num, unidb_lecturers.fname, unidb_lecturers.lname, unidb_students.fname, unidb_students.lname
FROM unidb_lecturers, unidb_students, unidb_courses
WHERE unidb_courses.num = '219';


-- Answers to exercise 2 questions
